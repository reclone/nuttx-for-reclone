/****************************************************************************
 * configs/reclone-rise/src/stm32_fpgacfg_upper.c
 *
 * This is a character device driver for configuring a Xilinx Spartan-6 FPGA
 * using a bitstream (.bit) file that gets dumped to this device.
 *
 *   Copyright (C) 2013, 2017-2019 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org> and Reclone Labs
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>

#include <stdint.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <debug.h>
#include <errno.h>
#include <arpa/inet.h>

#include <nuttx/kmalloc.h>
//#include <nuttx/fs/fs.h>
//#include <nuttx/audio/audio.h>
//#include <nuttx/audio/i2s.h>
#include "stm32_fpgacfg_lower.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Configuration ************************************************************/

//#ifndef CONFIG_AUDIO_I2SCHAR_RXTIMEOUT
//#  define CONFIG_AUDIO_I2SCHAR_RXTIMEOUT 0
//#endif
//
//#ifndef CONFIG_AUDIO_I2SCHAR_TXTIMEOUT
//#  define CONFIG_AUDIO_I2SCHAR_TXTIMEOUT 0
//#endif

/* Device naming ************************************************************/
#define DEVNAME_FMT    "/dev/fpgacfg%d"
#define DEVNAME_FMTLEN (12 + 3 + 1)

/* Constants**** ************************************************************/
#define FPGACFG_BITFILE_HEADER_LEN 14U
#define FPGACFG_BITSTREAM_HEADER_LEN 20U
#define FPGACFG_WRITEOUT_BUFFER_LEN 64U

/****************************************************************************
 * Private Types
 ****************************************************************************/

enum fpgacfg_state_e
{
  FPGACFG_DONE = 0,
  FPGACFG_INIT = 1,

  FPGACFG_BITHEADER,
  FPGACFG_DESIGNNAME,
  FPGACFG_PARTNAME,
  FPGACFG_SYNTHDATE,
  FPGACFG_SYNTHTIME,
  FPGACFG_BITSTREAMSIZE,

  FPGACFG_SYNCBYTES,
  FPGACFG_BITHEADERWORD,
  FPGACFG_BITPACKETLENGTH,
  FPGACFG_BITPACKETDATA,

  FPGACFG_BADFILEFORMAT,
  FPGACFG_INITIATE_FAILED,
  FPGACFG_SEND_FAILED,
  FPGACFG_FINALIZE_FAILED
};


struct fpgacfg_dev_s
{
  FAR struct fpgacfg_lowerhalf_s *lower;  /* The lower half fpgacfg driver */
  sem_t             exclsem;  /* Assures mutually exclusive access */
  uint8_t           crefs;    /* The number of times the device has been opened */
  enum fpgacfg_state_e state; /* State machine state */
  uint32_t          filepos; /* Processing position within the current file */
  uint32_t          fieldpos; /* Processing position within the current field */
  uint32_t          fieldlen; /* Length of a field */
  char              designname[60];
  char              partname[20];
  char              synthdate[12];
  char              synthtime[10];
  uint32_t          bitstreamlen; /* Length of the actual bitstream */
  uint32_t          bitfileheaderlen;
  uint16_t          bitpktheaderword;
  uint32_t          bitpktwordcount;
  uint8_t           writeoutbuf[FPGACFG_WRITEOUT_BUFFER_LEN];
  uint32_t          writeoutpos;
};


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */
static int     fpgacfg_open(FAR struct file *filep);
static int     fpgacfg_close(FAR struct file *filep);
static ssize_t fpgacfg_read(FAR struct file *filep, FAR char *buffer,
                 size_t buflen);
static ssize_t fpgacfg_write(FAR struct file *filep, FAR const char *buffer,
                 size_t buflen);


/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations fpgacfg_fops =
{
  fpgacfg_open,         /* open  */
  fpgacfg_close,        /* close */
  fpgacfg_read,         /* read  */
  fpgacfg_write,        /* write */
  NULL,                 /* seek  */
  NULL,                 /* ioctl */
#ifndef CONFIG_DISABLE_POLL
  NULL,                 /* poll  */
#endif
};

static const uint8_t EXPECTED_BITFILE_HEADER[FPGACFG_BITFILE_HEADER_LEN] =
    {0x00,0x09,0x0F,0xF0,0x0F,0xF0,0x0F,0xF0,0x0F,0xF0,0x00,0x00,0x01,0x61 };

static const uint8_t EXPECTED_BITSTREAM_HEADER[FPGACFG_BITSTREAM_HEADER_LEN] =
    {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
     0xAA, 0x99, 0x55, 0x66};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: fpgacfg_open
 *
 * Description:
 *   This function is called whenever the FPGA config device is opened.
 *
 ****************************************************************************/

static int fpgacfg_open(FAR struct file *filep)
{
  FAR struct inode           *inode = filep->f_inode;
  FAR struct fpgacfg_dev_s   *upper = inode->i_private;
  int                         ret;

  /* Get exclusive access to the device structures */

  ret = nxsem_wait(&upper->exclsem);
  if (ret < 0)
    {
      gpioerr("ERROR: nxsem_wait failed: %d\n", ret);
      DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
      goto errout;
    }

  if (0 != (filep->f_oflags & O_NONBLOCK))
    {
      /* Non-blocking mode probably won't work for this driver design */

      ret = -EWOULDBLOCK;
      goto errout_with_sem;
    }

  if (upper->crefs != 0)
    {
      /* Already open; only allow one open at a time */

      ret = -EMFILE;
      goto errout_with_sem;
    }

  /* Save the new open count on success */

  upper->crefs = 1;

  /* Initialize state machine */

  upper->state = FPGACFG_INIT;
  upper->filepos = 0;
  upper->fieldpos = 0;
  upper->fieldlen = 0;
  upper->designname[0] = '\0';
  upper->partname[0] = '\0';
  upper->synthdate[0] = '\0';
  upper->synthtime[0] = '\0';
  upper->bitstreamlen = 0;
  upper->bitfileheaderlen = 0;
  upper->bitpktheaderword = 0;
  upper->bitpktwordcount = 0;
  upper->writeoutpos = 0;

  ret = OK;

errout_with_sem:
  nxsem_post(&upper->exclsem);

errout:
  return ret;
}

/****************************************************************************
 * Name: fpgacfg_close
 *
 * Description:
 *   This function is called when the FPGA config device is closed.
 *
 ****************************************************************************/

static int fpgacfg_close(FAR struct file *filep)
{
  FAR struct inode           *inode = filep->f_inode;
  FAR struct fpgacfg_dev_s   *upper = inode->i_private;
  int                         ret;

  /* Get exclusive access to the device structures */

  ret = nxsem_wait(&upper->exclsem);
  if (ret < 0)
    {
      gpioerr("ERROR: nxsem_wait failed: %d\n", ret);
      DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
      goto errout;
    }

  /* Decrement the references to the driver.  If the reference count will
   * decrement to 0, then uninitialize the driver.
   */

  if (upper->crefs > 1)
    {
      upper->crefs--;
    }
  else
    {
      /* TODO uninitialize */
      upper->crefs = 0;
    }

  nxsem_post(&upper->exclsem);
  ret = OK;

errout:
  return ret;
}

/****************************************************************************
 * Name: fpgacfg_read
 *
 * Description:
 *   Standard character driver read method
 *
 ****************************************************************************/

static ssize_t fpgacfg_read(FAR struct file *filep, FAR char *buffer,
                            size_t buflen)
{
  /* Return zero -- usually meaning end-of-file */

  return 0;
}

/****************************************************************************
 * Name: fpgacfg_flush
 *
 * Description:
 *   Send any contents of the writeout buffer to the FPGA.
 *
 ****************************************************************************/
static int fpgacfg_flush(FAR struct fpgacfg_dev_s * priv)
{
  int ret;
  uint32_t bytes_to_send = FPGACFG_WRITEOUT_BUFFER_LEN;

  if (priv->writeoutpos == 0)
    {
      gpioerr("ERROR: fpgacfg_flush has nothing to send\n");
      return -EINVAL;
    }
  else if (priv->writeoutpos < FPGACFG_WRITEOUT_BUFFER_LEN)
    {
      bytes_to_send = priv->writeoutpos;
    }

  ret = priv->lower->ops->fpgacfg_send(
            priv->lower, priv->writeoutbuf, bytes_to_send);
  priv->writeoutpos = 0;

  return ret;
}

/****************************************************************************
 * Name: fpgacfg_send_bitstream_byte
 *
 * Description:
 *   Queue a single configuration bitstream byte for sending to the FPGA.
 *   When the send queue becomes full, then flush the whole buffer at once.
 *
 ****************************************************************************/
static int fpgacfg_send_bitstream_byte(FAR struct fpgacfg_dev_s * priv,
                                       uint8_t bitstream_byte)
{
  int ret = 0;

  if (priv->writeoutpos >= FPGACFG_WRITEOUT_BUFFER_LEN)
    {
      ret = fpgacfg_flush(priv);
      if (ret < 0)
        {
          gpioerr("ERROR: fpgacfg_flush failed: %d\n", ret);
          return ret;
        }
    }

  priv->writeoutbuf[priv->writeoutpos++] = bitstream_byte;

  return 0;
}

/****************************************************************************
 * Name: fpgacfg_write
 *
 * Description:
 *   Standard character driver write method
 *
 ****************************************************************************/

static ssize_t fpgacfg_write(FAR struct file *filep, FAR const char *buffer,
                             size_t buflen)
{
  FAR struct inode *inode;
  FAR struct fpgacfg_dev_s *priv;
  /*size_t nbytes;*/
  bool workdone;
  uint32_t bufpos;
  int ret;

  /*gpioinfo("buffer=%p buflen=%u\n", buffer, buflen);*/

  /* Get our private data structure */

  DEBUGASSERT(filep && buffer);

  inode = filep->f_inode;
  DEBUGASSERT(inode);

  priv = (FAR struct fpgacfg_dev_s *)inode->i_private;
  DEBUGASSERT(priv);

  /* Get exclusive access to the device structures */

  ret = nxsem_wait(&priv->exclsem);
  if (ret < 0)
    {
      gpioerr("ERROR: nxsem_wait failed: %d\n", ret);
      DEBUGASSERT(ret == -EINTR || ret == -ECANCELED);
      return ret;
    }

  /* Tick state machine */

  workdone = false;
  bufpos = 0;
  while (!workdone)
    {
      switch (priv->state)
      {
        case FPGACFG_BITHEADER:
          if (priv->fieldpos >= FPGACFG_BITFILE_HEADER_LEN)
            {
              /* Header was correct... move on to next piece */
              //gpioinfo("FPGACFG_DESIGNNAME\n");
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->state = FPGACFG_DESIGNNAME;
            }
          else if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (buffer[bufpos] != EXPECTED_BITFILE_HEADER[priv->fieldpos])
            {
              //gpioinfo("FPGACFG_BADFILEFORMAT\n");
              priv->state = FPGACFG_BADFILEFORMAT;
            }
          else
            {
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          break;

        case FPGACFG_DESIGNNAME:
          if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos < 2)
            {
              /* Grab a length byte */
              priv->fieldlen += (uint32_t)(buffer[bufpos]) << (8 * (1 - priv->fieldpos));
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else if (priv->fieldpos < (priv->fieldlen + 2))
            {
              if (priv->fieldlen > sizeof(priv->designname))
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
              else
                {
                  /* Copy the design name one character at a time */
                  priv->designname[priv->fieldpos - 2] = buffer[bufpos];
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
            }
          else
            {
              gpioinfo("Design name is '%.*s'\n", priv->fieldlen, priv->designname);
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->state = FPGACFG_PARTNAME;
            }
          break;

        case FPGACFG_PARTNAME:
          if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos == 0)
            {
              if (buffer[bufpos] == 'b')
                {
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
              else
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
            }
          else if (priv->fieldpos < 3)
            {
              /* Grab a length byte */
              priv->fieldlen += (uint32_t)(buffer[bufpos]) << (8 * (2 - priv->fieldpos));
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else if (priv->fieldpos < (priv->fieldlen + 3))
            {
              if (priv->fieldlen > sizeof(priv->partname))
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
              else
                {
                  /* Copy the part name one character at a time */
                  priv->partname[priv->fieldpos - 3] = buffer[bufpos];
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
            }
          else
            {
              gpioinfo("Part name is '%.*s'\n", priv->fieldlen, priv->partname);
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->state = FPGACFG_SYNTHDATE;
            }
          break;

        case FPGACFG_SYNTHDATE:
          if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos == 0)
            {
              if (buffer[bufpos] == 'c')
                {
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
              else
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
            }
          else if (priv->fieldpos < 3)
            {
              /* Grab a length byte */
              priv->fieldlen += (uint32_t)(buffer[bufpos]) << (8 * (2 - priv->fieldpos));
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else if (priv->fieldpos < (priv->fieldlen + 3))
            {
              if (priv->fieldlen > sizeof(priv->synthdate))
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
              else
                {
                  /* Copy the synth date one character at a time */
                  priv->synthdate[priv->fieldpos - 3] = buffer[bufpos];
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
            }
          else
            {
              gpioinfo("Synth date is '%.*s'\n", priv->fieldlen, priv->synthdate);
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->state = FPGACFG_SYNTHTIME;
            }
          break;

        case FPGACFG_SYNTHTIME:
          if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos == 0)
            {
              if (buffer[bufpos] == 'd')
                {
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
              else
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
            }
          else if (priv->fieldpos < 3)
            {
              /* Grab a length byte */
              priv->fieldlen += (uint32_t)(buffer[bufpos]) << (8 * (2 - priv->fieldpos));
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else if (priv->fieldpos < (priv->fieldlen + 3))
            {
              if (priv->fieldlen > sizeof(priv->synthtime))
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
              else
                {
                  /* Copy the synth time one character at a time */
                  priv->synthtime[priv->fieldpos - 3] = buffer[bufpos];
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
            }
          else
            {
              gpioinfo("Synth time is '%.*s'\n", priv->fieldlen, priv->synthtime);
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->bitstreamlen = 0;
              priv->state = FPGACFG_BITSTREAMSIZE;
            }
          break;

        case FPGACFG_BITSTREAMSIZE:
          if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos == 0)
            {
              if (buffer[bufpos] == 'e')
                {
                  ++priv->filepos;
                  ++priv->fieldpos;
                  ++bufpos;
                }
              else
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
            }
          else if (priv->fieldpos < 5)
            {
              /* Grab a length byte */
              priv->bitstreamlen += (uint32_t)(buffer[bufpos]) << (8 * (4 - priv->fieldpos));
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else
            {
              gpioinfo("Bitstream is %u bytes\n", priv->bitstreamlen);
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->bitfileheaderlen = priv->filepos;
              gpioinfo("Bit file header was %u bytes\n", priv->bitfileheaderlen);
              ret = priv->lower->ops->fpgacfg_initiate(priv->lower);
              if (0 == ret)
                {
                  priv->state = FPGACFG_SYNCBYTES;
                }
              else
                {
                  gpioerr("FPGA config initiation failed with return code %d!\n", ret);
                  priv->state = FPGACFG_INITIATE_FAILED;
                }
            }
          break;

        case FPGACFG_SYNCBYTES:
          if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos >= FPGACFG_BITSTREAM_HEADER_LEN)
            {
              /* Header was correct... move on to next piece */
              gpioinfo("Good sync bytes\n");
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->bitpktheaderword = 0;
              priv->state = FPGACFG_BITHEADERWORD;
            }
          else if (buffer[bufpos] != EXPECTED_BITSTREAM_HEADER[priv->fieldpos])
            {
              gpioinfo("Bad sync bytes\n");
              priv->state = FPGACFG_BADFILEFORMAT;
            }
          else
            {
              ret = fpgacfg_send_bitstream_byte(priv, buffer[bufpos]);
              if (0 != ret)
                {
                  gpioerr("FPGA config send byte failed with return code %d!\n", ret);
                  priv->state = FPGACFG_SEND_FAILED;
                }
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          break;

        case FPGACFG_BITHEADERWORD:
          if (priv->filepos >= priv->bitfileheaderlen + priv->bitstreamlen)
            {
              /* Flush whatever is remaining in the send buffer */
              ret = fpgacfg_flush(priv);
              if (0 == ret)
                {
                  ret = priv->lower->ops->fpgacfg_finalize(priv->lower);
                  if (0 == ret)
                    {
                      gpioinfo("Done!\n");
                      priv->state = FPGACFG_DONE;
                    }
                  else
                    {
                      gpioerr("FPGA config finalize failed with return code %d!\n", ret);
                      priv->state = FPGACFG_FINALIZE_FAILED;
                    }
                }
              else
                {
                  gpioerr("FPGA config send failed with return code %d!\n", ret);
                  priv->state = FPGACFG_SEND_FAILED;
                }
            }
          else if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos < 2)
            {
              /* Grab a length byte */
              priv->bitpktheaderword += (uint32_t)(buffer[bufpos]) << (8 * (1 - priv->fieldpos));
              ret = fpgacfg_send_bitstream_byte(priv, buffer[bufpos]);
              if (0 != ret)
                {
                  gpioerr("FPGA config send byte failed with return code %d!\n", ret);
                  priv->state = FPGACFG_SEND_FAILED;
                }
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else
            {
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              if (((priv->bitpktheaderword >> 13) & 0x0007) == 1U)
                {
                  /* Type 1 header - word count is part of header */
                  priv->bitpktwordcount = priv->bitpktheaderword & 0x001F;
                  priv->state = FPGACFG_BITPACKETDATA;
                }
              else if (((priv->bitpktheaderword >> 13) & 0x0007) == 2U)
                {
                  /* Type 2 header - word count follows header */
                  priv->bitpktwordcount = 0;
                  priv->state = FPGACFG_BITPACKETLENGTH;
                }
              else
                {
                  priv->state = FPGACFG_BADFILEFORMAT;
                }
            }
          break;

        case FPGACFG_BITPACKETLENGTH:
          if (priv->fieldpos < 4 &&
              priv->filepos >= priv->bitfileheaderlen + priv->bitstreamlen)
            {
              /* File ended before we got the whole integer */
              priv->state = FPGACFG_BADFILEFORMAT;
            }
          else if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos < 4)
            {
              /* Grab a length byte */
              priv->bitpktwordcount += (uint32_t)(buffer[bufpos]) << (8 * (3 - priv->fieldpos));
              ret = fpgacfg_send_bitstream_byte(priv, buffer[bufpos]);
              if (0 != ret)
                {
                  gpioerr("FPGA config send byte failed with return code %d!\n", ret);
                  priv->state = FPGACFG_SEND_FAILED;
                }
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else
            {
              priv->bitpktwordcount += 2;
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->state = FPGACFG_BITPACKETDATA;
            }
          break;

        case FPGACFG_BITPACKETDATA:
          if (priv->fieldpos < priv->bitpktwordcount * 2 &&
              priv->filepos >= priv->bitfileheaderlen + priv->bitstreamlen)
            {
              /* File ended before we got the whole packet */
              priv->state = FPGACFG_BADFILEFORMAT;
            }
          else if (bufpos >= buflen)
            {
              /* Work is done for now... not enough buffer to finish this piece */
              workdone = true;
            }
          else if (priv->fieldpos < priv->bitpktwordcount * 2)
            {
              /* Skip the packet data bytes */
              ret = fpgacfg_send_bitstream_byte(priv, buffer[bufpos]);
              if (0 != ret)
                {
                  gpioerr("FPGA config send byte failed with return code %d!\n", ret);
                  priv->state = FPGACFG_SEND_FAILED;
                }
              ++priv->filepos;
              ++priv->fieldpos;
              ++bufpos;
            }
          else
            {
              priv->fieldpos = 0;
              priv->fieldlen = 0;
              priv->bitpktheaderword = 0;
              priv->state = FPGACFG_BITHEADERWORD;
            }
          break;

        case FPGACFG_DONE:
          /* Terminal state */
          workdone = true;
          break;

        case FPGACFG_BADFILEFORMAT:
          /* Terminal state */
          workdone = true;
          break;

        case FPGACFG_INITIATE_FAILED:
          /* Terminal state */
          workdone = true;
          break;

        case FPGACFG_SEND_FAILED:
          /* Terminal state */
          workdone = true;
          break;

        case FPGACFG_FINALIZE_FAILED:
          /* Terminal state */
          workdone = true;
          break;

        case FPGACFG_INIT:
        default:
          /* TODO: Initialize lower-half driver */

          priv->filepos = 0;
          priv->designname[0] = '\0';
          priv->partname[0] = '\0';
          priv->synthdate[0] = '\0';
          priv->synthtime[0] = '\0';
          priv->bitstreamlen = 0;

          priv->state = FPGACFG_BITHEADER;
          break;
      }
    }

  nxsem_post(&priv->exclsem);
  return (ssize_t)buflen;
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: fpgacfg_register
 *
 * Description:
 *   Create and register the FPGA configuration character driver.
 *
 *   TODO description
 *
 * Input Parameters:
 *   minor - The device minor number.  The fpgacfg character device will be
 *     registered as /dev/fpgacfgN where N is the minor number
 *
 * Returned Value:
 *   OK if the driver was successfully registered; A negated errno value is
 *   returned on any failure.
 *
 ****************************************************************************/

int fpgacfg_register(FAR struct fpgacfg_lowerhalf_s * lower, int minor)
{
  FAR struct fpgacfg_dev_s *priv;
  char devname[DEVNAME_FMTLEN];
  int ret;

  /* Sanity check */

  DEBUGASSERT((unsigned)minor < 1000);
  DEBUGASSERT(lower != NULL);

  /* Allocate a FPGA config character device structure */

  priv = (FAR struct fpgacfg_dev_s *)kmm_zalloc(sizeof(struct fpgacfg_dev_s));
  if (priv)
    {
      /* Initialize the FPGA config device structure */

      priv->lower = lower;
      nxsem_init(&priv->exclsem, 0, 1);

      /* Create the character device name */

      snprintf(devname, DEVNAME_FMTLEN, DEVNAME_FMT, minor);
      ret = register_driver(devname, &fpgacfg_fops, 0666, priv);
      if (ret < 0)
        {
          /* Free the device structure if we failed to create the character
           * device.
           */

          kmm_free(priv);
          return ret;
        }

      /* Return the result of the registration */

      return OK;
    }


  return -ENOMEM;
}
