/****************************************************************************
 * configs/reclone-rise/src/stm32_fpgacfg_lower.h
 *
 *   Copyright (C) 2019 Reclone Labs. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef CONFIGS_RECLONE_RISE_SRC_STM32_FPGACFG_LOWER_H_
#define CONFIGS_RECLONE_RISE_SRC_STM32_FPGACFG_LOWER_H_

#include <stdint.h>
#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <nuttx/audio/audio.h>



/* Typedef for lower-level to upper-level callback for buffer dequeuing */

/* FIXME might not need this, because we are requiring blocking access?
typedef CODE void (*fpgacfg_callback_t)(FAR void *priv, uint16_t reason,
        FAR struct ap_buffer_s *apb, uint16_t status); */


/* This structure is the generic form of state structure used by lower half
 * FPGA config driver.  This state structure is passed to the FPGA config
 * upper-half driver when the driver is initialized.  Then, on subsequent
 * callbacks into the lower half FPGA config logic, this structure is
 * provided so that the Audio logic can maintain state information.
 *
 * Normally that FPGA config logic will have its own, custom state structure
 * that is simply cast to struct audio_lowerhalf_s.  In order to perform such
 * casts, the initial fields of the custom state structure match the initial
 * fields of the following generic FPGA config state structure.
 */

struct fpgacfg_lowerhalf_s
{
  /* The first field of this state structure must be a pointer to the
   * FPGA config callback structure:
   */

  FAR const struct fpgacfg_ops_s *ops;

  /* The bind data to the upper-half driver used for callbacks of dequeuing
   * buffer, reporting asynchronous events, reporting errors, etc.
   */

  /* FAR fpgacfg_callback_t  upper; */

  /* The private opaque pointer to be passed to upper-layer during callbacks */

  /* FAR void *priv; */

  /* The custom FPGA config lower-half device state structure may include
   * additional fields after the pointer to the FPGA config callback structure.
   */
};


/* The FPGA config vtable */

struct fpgacfg_ops_s
{
  CODE int      (*fpgacfg_initiate)(FAR struct fpgacfg_lowerhalf_s *dev);
  CODE int      (*fpgacfg_send)(FAR struct fpgacfg_lowerhalf_s *dev,
                  FAR uint8_t * bitstream_data, uint32_t data_len);
  CODE int      (*fpgacfg_abort)(FAR struct fpgacfg_lowerhalf_s *dev);
  CODE int      (*fpgacfg_finalize)(FAR struct fpgacfg_lowerhalf_s *dev);
};




FAR struct fpgacfg_lowerhalf_s *fpgacfg_dev_initialize(
    uint32_t program_b_pin, uint32_t init_b_pin, uint32_t cfg_done_pin,
    uint32_t din_pin, uint32_t cclk_pin);


#endif /* CONFIGS_RECLONE_RISE_SRC_STM32_FPGACFG_LOWER_H_ */
