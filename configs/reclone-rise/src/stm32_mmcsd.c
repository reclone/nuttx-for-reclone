/*****************************************************************************
 * configs/stm32f103-minimum/src/stm32_mmcsd.c
 *
 *   Copyright (C) 2017 Greg Nutt. All rights reserved.
 *   Author: Alan Carvalho de Assis <acassis@gmail.com> and Reclone Labs
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ****************************************************************************/

/*****************************************************************************
 * Included Files
 ****************************************************************************/

#include <debug.h>
#include <nuttx/config.h>
#include <nuttx/mmcsd.h>
#include <nuttx/spi/spi.h>
#include <nuttx/kthread.h>
#include <pthread.h>
#include <sched.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#include "stm32.h"
#include "reclonerise.h"
#include "stm32_spi.h"

/*****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef CONFIG_STM32_SPI1
#  error "SD driver requires CONFIG_STM32_SPI1 to be enabled"
#endif

#ifdef CONFIG_DISABLE_MOUNTPOINT
#  error "SD driver requires CONFIG_DISABLE_MOUNTPOINT to be disabled"
#endif


/*****************************************************************************
 * Private Definitions
 ****************************************************************************/

static const int SDMMC_SPI_PORT = 1; /* SD is connected to SPI1 port */

static bool g_sdmmc_card_detect = FALSE; /* SD card detect state */
static spi_mediachange_t g_sdmmc_spi_cb = NULL; /* Media change callback */
static FAR void * g_sdmmc_spi_cbarg = NULL;  /* Opaque pointer argument for media change callback */

/* Signal this PID to inform thread that card was inserted or pulled out */
static pid_t g_sdmmc_cd_thread_pid = -1;


/*****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_cardinserted_internal
 *
 * Description:
 *   Check if a card is inserted into the selected MMCSD slot
 *
 ****************************************************************************/

static bool stm32_cardinserted_internal(void)
{
  bool inserted;

  /* Get the state of the GPIO pin */

  inserted = !stm32_gpioread(GPIO_MMCSD_NCD);
  finfo("Slot %d inserted: %s\n", MMCSD_SLOTNO, inserted ? "YES" : "NO");
  return inserted;
}

/*****************************************************************************
 * Name: stm32_cd_thread
 *
 * Description:
 *   Working thread to call media changed callback when card is inserted or
 *   pulled out.  Debounces the card detect input.
 ****************************************************************************/

static int stm32_mmcsd_carddetect_thread(int argc, FAR char *argv[])
{
  static const long debounce_interval_ms = 500;
  static const useconds_t polling_interval_us = 40000;
  static FAR sigset_t notify_sigset;
  bool last_sampled_cd;
  struct timespec last_seen_cd_change;
  long last_seen_cd_change_ms;
  bool current_cd;
  struct timespec nowish;
  long nowish_ms;

  (void)argc;
  (void)argv;


  while (1)
    {
      /* Wait for an interrupt to send us a signal */

      (void)sigemptyset(&notify_sigset);
      (void)sigaddset(&notify_sigset, SIGWORK);
      (void)nxsig_waitinfo(&notify_sigset, NULL);

      /* If we got the signal, an interrupt occurred.  Start polling every
       * polling_interval_us for a state change on the card detect line. */

      last_sampled_cd = stm32_cardinserted_internal();
      clock_gettime(CLOCK_MONOTONIC, &last_seen_cd_change);
      last_seen_cd_change_ms = last_seen_cd_change.tv_sec * 1000L +
                               last_seen_cd_change.tv_nsec / 1000000L;
      do
        {
          nxsig_usleep(polling_interval_us);

          current_cd = stm32_cardinserted_internal();
          clock_gettime(CLOCK_MONOTONIC, &nowish);
          nowish_ms = nowish.tv_sec * 1000L + nowish.tv_nsec / 1000000L;

          if (current_cd != last_sampled_cd)
            {
              last_seen_cd_change_ms = nowish_ms;
              last_sampled_cd = current_cd;
            }
        } while (nowish_ms - last_seen_cd_change_ms < debounce_interval_ms);

      /* At this point, the card detect line hasn't changed
       * for the last debounce_interval_ms milliseconds.  If the current state
       * is different from the debounced state, then update the debounced
       * state and trigger the media change callback. */

      if (g_sdmmc_card_detect != current_cd)
        {
          g_sdmmc_card_detect = current_cd;
          if (g_sdmmc_spi_cb)
            {
              g_sdmmc_spi_cb(g_sdmmc_spi_cbarg);
            }

#ifdef HAVE_AUTOMOUNTER
            /* Let the automounter know about the insertion event */

            stm32_automount_event(MMCSD_SLOTNO, g_sdmmc_card_detect);
#endif
        }

      /* Wait for another signal from the ISR. */
    }

  /* Should never get here */
  return 0;
}

/****************************************************************************
 * Name: stm32_mmcsd_carddetect
 *
 * Description:
 *   Interrupt service routine for card detect state changes
 *
 ****************************************************************************/

static int stm32_mmcsd_carddetect(int irq, FAR void *regs, FAR void *arg)
{
  /* Just signal and let the thread do its debouncing */
  (void)nxsig_kill(g_sdmmc_cd_thread_pid, SIGWORK);

  return OK;
}

/****************************************************************************
 * Name: stm32_mmcsd_setup
 *
 * Description:
 *   Configure SPI and set up for card detect events
 *
 ****************************************************************************/

static int stm32_mmcsd_setup(void)
{
  struct spi_dev_s *spi;
  static const char * cd_thread_name = "mmcsd_cd";
  static const int cd_thread_prio = 50;
  static const int cd_thread_stack_size = 512;
  int ret;
  /* Initialize the SPI bus and get an instance of the SPI interface */

  spi = stm32_spibus_initialize(SDMMC_SPI_PORT);
  if (spi == NULL)
    {
      spierr("ERROR: Failed to initialize SPI bus %d\n", SDMMC_SPI_PORT);
      return -ENODEV;
    }

  ret = mmcsd_spislotinitialize(MMCSD_MINOR, MMCSD_SLOTNO, spi);
  if (ret < 0)
    {
      mcerr("ERROR: Failed to bind SPI port %d to SD slot %d\n",
          SDMMC_SPI_PORT, MMCSD_SLOTNO);
      return ret;
    }

  /* Initialize Card Detect debounce thread */

  g_sdmmc_cd_thread_pid = kthread_create(cd_thread_name, cd_thread_prio,
                                         cd_thread_stack_size,
                                         stm32_mmcsd_carddetect_thread,
                                         (FAR char * const *)NULL);

  /* Initialize Card Detect pin and enable interrupt on edges */

  stm32_configgpio(GPIO_MMCSD_NCD);
  stm32_gpiosetevent(GPIO_MMCSD_NCD, true, true, true, stm32_mmcsd_carddetect, NULL);

  g_sdmmc_card_detect =  stm32_cardinserted_internal();
  if (g_sdmmc_spi_cb)
    {
      g_sdmmc_spi_cb(g_sdmmc_spi_cbarg);
    }

#ifdef HAVE_AUTOMOUNTER
  /* Let the automounter know about the insertion event */

  stm32_automount_event(MMCSD_SLOTNO, stm32_cardinserted());
#endif

  mcinfo("INFO: mmcsd%d card has been initialized successfully\n", MMCSD_MINOR);
  return OK;
}

/*****************************************************************************
 * Public Functions
 ****************************************************************************/

/*****************************************************************************
 * Name: stm32_spi1register
 *
 * Description:
 *   Registers media change callback
 ****************************************************************************/

int stm32_spi1register(struct spi_dev_s *dev, spi_mediachange_t callback,
                       void *arg)
{
  spiinfo("INFO: Registering spi1 device\n");
  g_sdmmc_spi_cb = callback;
  g_sdmmc_spi_cbarg = arg;
  return OK;
}

/****************************************************************************
 * Name: stm32_cardinserted
 *
 * Description:
 *   Check if a card is inserted into the selected MMCSD slot
 *
 ****************************************************************************/

bool stm32_cardinserted(void)
{
  /* Return the state of the CD pin */

  return stm32_cardinserted_internal();
}

/*****************************************************************************
 * Name: stm32_mmcsd_initialize
 *
 * Description:
 *   Initialize SPI-based SD card and card detect thread.
 ****************************************************************************/

int stm32_mmcsd_initialize(int minor)
{
  int ret;

  finfo("Configuring MMCSD\n");

  ret = stm32_mmcsd_setup();
  if (ret < 0)
    {
      mcerr("ERROR: Failed to initialize MMCSD: %d\n", ret);
    }

  return OK;
}
