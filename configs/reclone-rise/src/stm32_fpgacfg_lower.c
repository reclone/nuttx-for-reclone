/****************************************************************************
 * configs/reclone-rise/src/stm32_fpgacfg_lower.c
 *
 *   Copyright (C) 2019 Reclone Labs. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <string.h>
#include <nuttx/config.h>
#include <nuttx/kmalloc.h>
#include "stm32_gpio.h"
#include "stm32_fpgacfg_lower.h"

/****************************************************************************
 * Private Type Definitions
 ****************************************************************************/

struct fpgacfg_xc6ss_s
{
    struct fpgacfg_lowerhalf_s lower;
    uint32_t total_bytes;
    uint32_t program_b_pin;
    uint32_t init_b_pin;
    uint32_t cfg_done_pin;
    uint32_t din_pin;
    uint32_t cclk_pin;
};


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/
static int fpgacfg_dev_initiate(FAR struct fpgacfg_lowerhalf_s *dev);
static int fpgacfg_dev_send(FAR struct fpgacfg_lowerhalf_s *dev,
                FAR uint8_t * bitstream_data, uint32_t data_len);
static int fpgacfg_dev_abort(FAR struct fpgacfg_lowerhalf_s *dev);
static int fpgacfg_dev_finalize(FAR struct fpgacfg_lowerhalf_s *dev);


/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct fpgacfg_ops_s fpgacfg_device_ops =
{
    fpgacfg_dev_initiate,
    fpgacfg_dev_send,
    fpgacfg_dev_abort,
    fpgacfg_dev_finalize
};


/****************************************************************************
 * Private Functions
 ****************************************************************************/

static int fpgacfg_dev_initiate(FAR struct fpgacfg_lowerhalf_s *dev)
{
  FAR struct fpgacfg_xc6ss_s * xc6ss_dev = (FAR struct fpgacfg_xc6ss_s *)dev;
  bool init_b_state;

  gpioinfo("called\n");

  /* Re-initialize DIN and CCLK pins just in case they were left in a non-default state */
  stm32_configgpio(xc6ss_dev->din_pin);
  stm32_configgpio(xc6ss_dev->cclk_pin);

  /* Go through normal slave serial config init sequence -
   * pulse PROGRAM_B low and make sure INIT_B goes low then high */
  stm32_gpiowrite(xc6ss_dev->program_b_pin, false);
  nxsig_usleep(10000);

  init_b_state = stm32_gpioread(xc6ss_dev->init_b_pin);
  if (true == init_b_state)
    {
      gpioerr("init_b did not go low\n");
      return -ETIME;
    }

  stm32_gpiowrite(xc6ss_dev->program_b_pin, true);
  nxsig_usleep(10000);

  init_b_state = stm32_gpioread(xc6ss_dev->init_b_pin);
  if (false == init_b_state)
    {
      gpioerr("init_b did not return high\n");
      return -ETIME;
    }

  gpioinfo("fpga ready to receive bitstream\n");
  xc6ss_dev->total_bytes = 0;

  return 0;
}

static int fpgacfg_dev_sendbit(FAR struct fpgacfg_lowerhalf_s *dev, bool cfg_bit)
{
  FAR struct fpgacfg_xc6ss_s * xc6ss_dev = (FAR struct fpgacfg_xc6ss_s *)dev;
  int ret = 0;

  stm32_gpiowrite(xc6ss_dev->cclk_pin, false);
  stm32_gpiowrite(xc6ss_dev->din_pin, cfg_bit);

  asm("nop");
  asm("nop");
  asm("nop");

  stm32_gpiowrite(xc6ss_dev->cclk_pin, true);

  asm("nop");
  asm("nop");
  asm("nop");

  return ret;
}

static int fpgacfg_dev_sendbyte(FAR struct fpgacfg_lowerhalf_s *dev, uint8_t cfg_byte)
{
  int ret = 0;
  unsigned int i;

  /* Send one bit at a time, MSb first */
  for (i = 0; i <= 7; ++i)
    {
      fpgacfg_dev_sendbit(dev, (cfg_byte & (0x80 >> i)) != 0);
    }


  return ret;
}

static int fpgacfg_dev_send(FAR struct fpgacfg_lowerhalf_s *dev,
                FAR uint8_t * bitstream_data, uint32_t data_len)
{
  FAR struct fpgacfg_xc6ss_s * xc6ss_dev = (FAR struct fpgacfg_xc6ss_s *)dev;
  uint32_t i;
  int ret = 0;

  /* Send one byte at a time */
  for (i = 0; i < data_len && ret >= 0; ++i)
    {
      ret = fpgacfg_dev_sendbyte(dev, bitstream_data[i]);
      ++xc6ss_dev->total_bytes;
    }

  return ret;
}

static int fpgacfg_dev_abort(FAR struct fpgacfg_lowerhalf_s *dev)
{
  FAR struct fpgacfg_xc6ss_s * xc6ss_dev = (FAR struct fpgacfg_xc6ss_s *)dev;
  gpioinfo("called\n");

  /* Just pull PROGRAM_B low to reset the FPGA */
  stm32_gpiowrite(xc6ss_dev->program_b_pin, false);

  return 0;
}

static int fpgacfg_dev_finalize(FAR struct fpgacfg_lowerhalf_s *dev)
{
  FAR struct fpgacfg_xc6ss_s * xc6ss_dev = (FAR struct fpgacfg_xc6ss_s *)dev;
  int ret = 0;
  bool init_b_state;
  bool cfg_done_state;

  gpioinfo("called; total_bytes=%u\n", xc6ss_dev->total_bytes);

  /* Verify CRC passed and FPGA shows DONE */
  init_b_state = stm32_gpioread(xc6ss_dev->init_b_pin);
  cfg_done_state = stm32_gpioread(xc6ss_dev->cfg_done_pin);
  gpioinfo("INIT_B=%d  DONE=%d\n", init_b_state ? 1 : 0, cfg_done_state ? 1 : 0);

  if (!init_b_state || !cfg_done_state)
    {
      gpioerr("FPGA Configuration was unsuccessful! (INIT_B=%d DONE=%d)\n", init_b_state ? 1 : 0, cfg_done_state ? 1 : 0);
      ret = -EINVAL;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

FAR struct fpgacfg_lowerhalf_s *fpgacfg_dev_initialize(
    uint32_t program_b_pin, uint32_t init_b_pin, uint32_t cfg_done_pin, uint32_t din_pin, uint32_t cclk_pin)
{
    FAR struct fpgacfg_xc6ss_s * ret;

    /* Allocate a FPGA config lower half structure */

    ret = (FAR struct fpgacfg_xc6ss_s *)kmm_zalloc(sizeof(struct fpgacfg_xc6ss_s));
    if (ret)
      {
        ret->lower.ops = &fpgacfg_device_ops;
        ret->program_b_pin = program_b_pin;
        ret->init_b_pin = init_b_pin;
        ret->cfg_done_pin = cfg_done_pin;
        ret->din_pin = din_pin;
        ret->cclk_pin = cclk_pin;
      }

    /* Initialize required GPIO pins */

    stm32_configgpio(program_b_pin);
    stm32_configgpio(init_b_pin);
    stm32_configgpio(cfg_done_pin);
    stm32_configgpio(din_pin);
    stm32_configgpio(cclk_pin);

    return (FAR struct fpgacfg_lowerhalf_s *)ret;
}




